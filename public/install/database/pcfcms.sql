/*
MySQL Backup
Source Server Version: 5.7.30
Source Database: pcfcms
Date: 2021/8/24 15:44:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `pcf_ad`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_ad`;
CREATE TABLE `pcf_ad` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '广告位置ID',
  `media_type` tinyint(1) DEFAULT '0' COMMENT '广告类型',
  `title` varchar(60) DEFAULT '' COMMENT '广告名称',
  `links` varchar(255) DEFAULT '' COMMENT '广告链接',
  `litpic` varchar(255) DEFAULT '' COMMENT '图片地址',
  `start_time` int(11) DEFAULT NULL COMMENT '投放时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `intro` text COMMENT '描述',
  `link_man` varchar(60) DEFAULT '' COMMENT '添加人',
  `link_email` varchar(60) DEFAULT '' COMMENT '添加人邮箱',
  `link_phone` varchar(60) DEFAULT '' COMMENT '添加人联系电话',
  `click` int(11) DEFAULT '0' COMMENT '点击量',
  `bgcolor` varchar(30) DEFAULT '' COMMENT '背景颜色',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '1=显示，0=屏蔽',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序',
  `target` varchar(50) DEFAULT '' COMMENT '是否开启浏览器新窗口',
  `admin_id` int(10) DEFAULT '0' COMMENT '管理员ID',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `position_id` (`pid`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='广告表';

-- ----------------------------
--  Table structure for `pcf_admin`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_admin`;
CREATE TABLE `pcf_admin` (
  `admin_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `pen_name` varchar(50) DEFAULT '' COMMENT '笔名（发布文章后显示责任编辑的名字）',
  `sex` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号码',
  `email` varchar(60) DEFAULT '' COMMENT 'email',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `head_pic` varchar(255) DEFAULT '' COMMENT '头像',
  `last_login` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) DEFAULT '' COMMENT '最后登录ip',
  `login_cnt` int(11) DEFAULT '0' COMMENT '登录次数',
  `role_id` int(10) NOT NULL DEFAULT '-1' COMMENT '角色组ID（-1表示超级管理员）',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `parent_id` int(10) DEFAULT '0' COMMENT '父id',
  `typeid` int(11) unsigned DEFAULT '0' COMMENT '默认栏目id',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`admin_id`),
  KEY `user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
--  Table structure for `pcf_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_admin_log`;
CREATE TABLE `pcf_admin_log` (
  `log_id` bigint(16) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `admin_id` int(10) NOT NULL DEFAULT '-1' COMMENT '管理员id',
  `log_info` text COMMENT '日志描述',
  `log_ip` varchar(30) DEFAULT '' COMMENT 'ip地址',
  `log_osName` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `log_device` varchar(255) DEFAULT '' COMMENT '设备',
  `log_time` int(11) DEFAULT '0' COMMENT '日志时间',
  `log_browserType` varchar(255) DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`log_id`),
  KEY `admin_id` (`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8 COMMENT='操作日志表';

-- ----------------------------
--  Table structure for `pcf_ad_position`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_ad_position`;
CREATE TABLE `pcf_ad_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '广告位置名称',
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0关闭1开启',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='广告位置表';

-- ----------------------------
--  Table structure for `pcf_archives`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_archives`;
CREATE TABLE `pcf_archives` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `joinaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联文档id',
  `typeid` int(10) NOT NULL DEFAULT '0' COMMENT '当前栏目',
  `channel` int(10) NOT NULL DEFAULT '0' COMMENT '模型ID',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `litpic` varchar(250) NOT NULL DEFAULT '' COMMENT '缩略图',
  `is_b` tinyint(1) NOT NULL DEFAULT '0' COMMENT '加粗',
  `is_head` tinyint(1) NOT NULL DEFAULT '0' COMMENT '头条（0=否，1=是）',
  `is_special` tinyint(1) NOT NULL DEFAULT '0' COMMENT '特荐（0=否，1=是）',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶（0=否，1=是）',
  `is_recom` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐（0=否，1=是）',
  `is_jump` tinyint(1) NOT NULL DEFAULT '0' COMMENT '跳转链接（0=否，1=是）',
  `is_litpic` tinyint(1) NOT NULL DEFAULT '0' COMMENT '图片（0=否，1=是）',
  `author` varchar(200) NOT NULL DEFAULT '' COMMENT '作者',
  `click` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `arcrank` tinyint(1) NOT NULL DEFAULT '0' COMMENT '阅读权限：0=开放浏览，-1=待审核稿件',
  `jumplinks` varchar(200) NOT NULL DEFAULT '' COMMENT '外链跳转',
  `seo_title` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO关键词',
  `seo_description` text NOT NULL COMMENT 'SEO描述',
  `tempview` varchar(200) NOT NULL DEFAULT '' COMMENT '文档模板文件名',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序号',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `users_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `show_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '置顶时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`aid`),
  KEY `typeid` (`typeid`,`channel`,`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=434 DEFAULT CHARSET=utf8 COMMENT='文档主表';

-- ----------------------------
--  Table structure for `pcf_arcrank`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_arcrank`;
CREATE TABLE `pcf_arcrank` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `rank` smallint(6) DEFAULT '0' COMMENT '权限值',
  `name` char(20) DEFAULT '' COMMENT '会员名称',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文档阅读权限表';

-- ----------------------------
--  Table structure for `pcf_arctype`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_arctype`;
CREATE TABLE `pcf_arctype` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `channeltype` int(10) DEFAULT '0' COMMENT '栏目顶级模型ID',
  `current_channel` int(10) DEFAULT '0' COMMENT '栏目当前模型ID',
  `parent_id` int(10) DEFAULT '0' COMMENT '栏目上级ID',
  `typename` varchar(200) DEFAULT '' COMMENT '栏目名称',
  `dirname` varchar(200) DEFAULT '' COMMENT '目录英文名',
  `dirpath` varchar(200) DEFAULT '' COMMENT '目录存放HTML路径',
  `englist_name` varchar(200) DEFAULT '' COMMENT '栏目英文名',
  `grade` tinyint(1) DEFAULT '0' COMMENT '栏目等级',
  `typelink` varchar(200) DEFAULT '' COMMENT '栏目链接',
  `litpic` varchar(250) DEFAULT '' COMMENT '栏目图片',
  `templist` varchar(200) DEFAULT '' COMMENT '列表模板文件名',
  `tempview` varchar(200) DEFAULT '' COMMENT '文档模板文件名',
  `seo_title` varchar(200) DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) DEFAULT '' COMMENT 'seo关键字',
  `seo_description` text COMMENT 'seo描述',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `is_hidden` tinyint(1) DEFAULT '0' COMMENT '是否隐藏栏目：0=显示，1=隐藏',
  `is_part` tinyint(1) DEFAULT '0' COMMENT '栏目属性：0=内容栏目，1=外部链接',
  `admin_id` int(10) DEFAULT '0' COMMENT '管理员ID',
  `status` tinyint(1) DEFAULT '1' COMMENT '启用 (1=正常，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dirname` (`dirname`) USING BTREE,
  KEY `parent_id` (`channeltype`,`parent_id`) USING BTREE,
  KEY `parent_id_2` (`parent_id`,`sort_order`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8 COMMENT='分类栏目表';

-- ----------------------------
--  Table structure for `pcf_article_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_article_content`;
CREATE TABLE `pcf_article_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `typeid` int(11) DEFAULT NULL,
  `content` longtext NOT NULL COMMENT '内容详情',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='文章附加表';

-- ----------------------------
--  Table structure for `pcf_auth_role`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_auth_role`;
CREATE TABLE `pcf_auth_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '' COMMENT '角色名',
  `remark` text COMMENT '备注信息',
  `permission` text COMMENT '已允许的权限',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=正常，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `online_update` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '在线升级',
  `only_oneself` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '只查看自己发布',
  `built_in` tinyint(1) DEFAULT '0' COMMENT '内置用户组，1表示内置',
  `admin_id` int(10) DEFAULT '0' COMMENT '操作员',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员角色表';

-- ----------------------------
--  Table structure for `pcf_channelfield`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channelfield`;
CREATE TABLE `pcf_channelfield` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '字段名称',
  `channel_id` int(10) NOT NULL DEFAULT '0' COMMENT '所属文档模型id',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '字段标题',
  `dtype` varchar(32) NOT NULL DEFAULT '' COMMENT '字段类型',
  `define` text NOT NULL COMMENT '字段定义',
  `maxlength` int(10) NOT NULL DEFAULT '0' COMMENT '最大长度，文本数据必须填写，大于255为text类型',
  `dfvalue` text NOT NULL COMMENT '默认值',
  `is_screening` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否应用于条件筛选',
  `is_order` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否应用于排序',
  `ifsystem` tinyint(1) NOT NULL DEFAULT '0' COMMENT '字段分类，1=系统(不可修改)，0=自定义',
  `ifrequire` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
  `ifmain` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否主表字段',
  `ifeditable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在编辑页显示',
  `ifcontrol` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，控制该条数据是否允许被控制，1为不允许控制，0为允许控制',
  `sort_order` int(5) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `channel_id` (`channel_id`,`is_screening`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=556 DEFAULT CHARSET=utf8 COMMENT='自定义字段表';

-- ----------------------------
--  Table structure for `pcf_channelfield_bind`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channelfield_bind`;
CREATE TABLE `pcf_channelfield_bind` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `typeid` int(10) DEFAULT '0' COMMENT '栏目ID',
  `field_id` int(10) DEFAULT '0' COMMENT '自定义字段ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型，1：筛选，2：排序',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='栏目与自定义字段绑定表';

-- ----------------------------
--  Table structure for `pcf_channel_type`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channel_type`;
CREATE TABLE `pcf_channel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` varchar(50) NOT NULL DEFAULT '' COMMENT '识别id',
  `title` varchar(30) DEFAULT '' COMMENT '名称',
  `ntitle` varchar(30) DEFAULT '' COMMENT '左侧菜单名称',
  `table` varchar(50) DEFAULT '' COMMENT '表名',
  `ctl_name` varchar(50) DEFAULT '' COMMENT '控制器名称（区分大小写）',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=启用，0=屏蔽)',
  `ifsystem` tinyint(1) DEFAULT '0' COMMENT '字段分类，1=系统(不可修改)，0=自定义',
  `is_repeat_title` tinyint(1) DEFAULT '1' COMMENT '文档标题重复，1=允许，0=不允许',
  `sort_order` smallint(6) DEFAULT '100' COMMENT '排序',
  `icon` varchar(255) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idention` (`nid`) USING BTREE,
  UNIQUE KEY `ctl_name` (`ctl_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='系统模型表';

-- ----------------------------
--  Table structure for `pcf_common_pic`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_common_pic`;
CREATE TABLE `pcf_common_pic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '常用图片ID',
  `pid` char(32) DEFAULT NULL,
  `pic_path` varchar(255) NOT NULL DEFAULT '' COMMENT '图片地址',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='常用图片';

-- ----------------------------
--  Table structure for `pcf_config`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_config`;
CREATE TABLE `pcf_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '配置的key键名',
  `value` text,
  `inc_type` varchar(64) DEFAULT '' COMMENT '配置分组',
  `desc` varchar(50) DEFAULT '' COMMENT '描述',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `inc_type` (`inc_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=361 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
--  Table structure for `pcf_config_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_config_attribute`;
CREATE TABLE `pcf_config_attribute` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '表单id',
  `inc_type` varchar(20) DEFAULT '' COMMENT '变量分组',
  `attr_name` varchar(60) DEFAULT '' COMMENT '变量标题',
  `attr_var_name` varchar(50) DEFAULT '' COMMENT '变量名',
  `attr_input_type` tinyint(1) unsigned DEFAULT '0' COMMENT ' 0=文本框，1=下拉框，2=多行文本框，3=上传图片',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`attr_id`),
  KEY `inc_type` (`inc_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='自定义变量表';

-- ----------------------------
--  Table structure for `pcf_field_type`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_field_type`;
CREATE TABLE `pcf_field_type` (
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '字段类型',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '中文类型名',
  `ifoption` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要设置选项',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段类型表';

-- ----------------------------
--  Table structure for `pcf_guestbook`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook`;
CREATE TABLE `pcf_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `need_login` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='留言主表';

-- ----------------------------
--  Table structure for `pcf_guestbook_attr`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_attr`;
CREATE TABLE `pcf_guestbook_attr` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '表单id',
  `attr_name` varchar(60) DEFAULT '' COMMENT '字段名称',
  `form_id` int(11) unsigned DEFAULT '0' COMMENT '栏目ID',
  `input_type` varchar(32) DEFAULT '' COMMENT '字段类型',
  `attr_values` text COMMENT '可选值列表',
  `sort_order` int(11) unsigned DEFAULT '100' COMMENT '表单排序',
  `is_fill` tinyint(1) unsigned DEFAULT '0' COMMENT '是否必填，1：必填',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`attr_id`),
  KEY `form_id` (`form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='表单字段表';

-- ----------------------------
--  Table structure for `pcf_guestbook_list`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_list`;
CREATE TABLE `pcf_guestbook_list` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '表单id',
  `md5data` varchar(50) NOT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip所在城市',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`list_id`),
  KEY `form_id` (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单列表';

-- ----------------------------
--  Table structure for `pcf_guestbook_value`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_value`;
CREATE TABLE `pcf_guestbook_value` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '留言表单id自增',
  `form_id` int(11) unsigned NOT NULL DEFAULT '0',
  `list_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '留言id',
  `attr_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '表单id',
  `attr_value` text COMMENT '表单值',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`value_id`),
  KEY `form_id` (`form_id`,`list_id`,`attr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单内容表';

-- ----------------------------
--  Table structure for `pcf_images`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_images`;
CREATE TABLE `pcf_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` char(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图片ID',
  `name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图片名称',
  `url` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '绝对地址',
  `path` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '物理地址',
  `type` enum('web','local','Aliyun') CHARACTER SET utf8mb4 DEFAULT 'local' COMMENT '存储引擎',
  `ctime` bigint(12) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`pid`) USING BTREE,
  KEY `id_2` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片表';

-- ----------------------------
--  Table structure for `pcf_links`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_links`;
CREATE TABLE `pcf_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` tinyint(1) DEFAULT '1' COMMENT '类型：1=文字链接，2=图片链接',
  `title` varchar(50) DEFAULT '' COMMENT '网站标题',
  `url` varchar(100) DEFAULT '' COMMENT '网站地址',
  `logo` varchar(255) DEFAULT '' COMMENT '网站LOGO',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序号',
  `target` tinyint(1) DEFAULT '0' COMMENT '是否开启浏览器新窗口',
  `email` varchar(50) DEFAULT NULL,
  `intro` text COMMENT '网站简况',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=显示，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
--  Table structure for `pcf_media_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_media_content`;
CREATE TABLE `pcf_media_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `typeid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `content` longtext NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频表';

-- ----------------------------
--  Table structure for `pcf_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_menu`;
CREATE TABLE `pcf_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一性标识',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级ID',
  `name` varchar(30) CHARACTER SET utf8mb4 NOT NULL COMMENT '菜单名称',
  `icon` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图标',
  `url` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'URL地址',
  `param` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '参数',
  `auth` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '权限标识',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型：1模块 2导航 3菜单 4节点',
  `sort` smallint(5) unsigned DEFAULT '125' COMMENT '显示顺序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `is_show` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示：1显示 2不显示',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='节点管理表';

-- ----------------------------
--  Table structure for `pcf_nav_list`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_nav_list`;
CREATE TABLE `pcf_nav_list` (
  `nav_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '导航ID',
  `topid` int(10) NOT NULL DEFAULT '0' COMMENT '顶级菜单ID',
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `nav_name` varchar(200) NOT NULL DEFAULT '' COMMENT '导航名称',
  `englist_name` varchar(200) NOT NULL DEFAULT '' COMMENT '英文名称',
  `nav_url` varchar(200) NOT NULL DEFAULT '' COMMENT '导航链接',
  `position_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导航位置',
  `arctype_sync` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否与栏目同步',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '同步栏目的ID',
  `nav_pic` varchar(255) NOT NULL DEFAULT '' COMMENT '导航图片',
  `grade` tinyint(1) NOT NULL DEFAULT '0' COMMENT '菜单等级',
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否打开新窗口，1=是，0=否',
  `nofollow` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用nofollow，1=是，0=否',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '启用 (1=正常，0=停用)',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`nav_id`),
  KEY `type_id` (`type_id`),
  KEY `position_id` (`position_id`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='导航列表';

-- ----------------------------
--  Table structure for `pcf_nav_position`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_nav_position`;
CREATE TABLE `pcf_nav_position` (
  `position_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '导航列表ID',
  `position_name` varchar(200) NOT NULL DEFAULT '' COMMENT '导航列表名称',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序号',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='导航位置表';

-- ----------------------------
--  Table structure for `pcf_picture_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_picture_content`;
CREATE TABLE `pcf_picture_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `typeid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `content` longtext NOT NULL COMMENT '内容',
  `note` text NOT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='图片表';

-- ----------------------------
--  Table structure for `pcf_product_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_product_content`;
CREATE TABLE `pcf_product_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `typeid` int(10) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `content` longtext NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='产品表';

-- ----------------------------
--  Table structure for `pcf_quickentry`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_quickentry`;
CREATE TABLE `pcf_quickentry` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT '' COMMENT '名称',
  `laytext` varchar(50) DEFAULT '' COMMENT '完整标题',
  `controller` varchar(20) DEFAULT '' COMMENT '控制器名',
  `action` varchar(20) DEFAULT '' COMMENT '操作名',
  `vars` varchar(100) DEFAULT '' COMMENT 'URL参数字符串',
  `groups` smallint(5) DEFAULT '0' COMMENT '分组，1=模型',
  `checked` tinyint(4) DEFAULT '0' COMMENT '选中，0=否，1=是',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1=有效，0=无效',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `type` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='快捷入口表';

-- ----------------------------
--  Table structure for `pcf_search_word`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_search_word`;
CREATE TABLE `pcf_search_word` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `word` varchar(50) DEFAULT '' COMMENT '关键词',
  `searchNum` int(10) DEFAULT '1' COMMENT '搜索次数',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='搜索词统计表';

-- ----------------------------
--  Table structure for `pcf_single_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_single_content`;
CREATE TABLE `pcf_single_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `typeid` int(10) DEFAULT '0' COMMENT '栏目ID',
  `content` longtext NOT NULL COMMENT '内容详情',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='单页附加表';

-- ----------------------------
--  Table structure for `pcf_sms_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_sms_log`;
CREATE TABLE `pcf_sms_log` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `source` tinyint(1) DEFAULT '0' COMMENT '来源，与场景ID对应：0=默认，2=注册，3=绑定手机，4=找回密码',
  `mobile` varchar(50) DEFAULT '' COMMENT '手机号码',
  `users_id` int(10) DEFAULT '0' COMMENT '用户ID',
  `code` varchar(20) DEFAULT '' COMMENT '发送手机内容',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否使用，默认0，0为未使用，1为使用',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='手机发送记录表';

-- ----------------------------
--  Table structure for `pcf_smtp_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_smtp_log`;
CREATE TABLE `pcf_smtp_log` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `source` tinyint(1) DEFAULT '0' COMMENT '来源，与场景ID对应：0=默认，2=注册，3=绑定邮箱，4=找回密码',
  `email` varchar(50) DEFAULT '' COMMENT '邮件地址',
  `users_id` int(10) DEFAULT '0' COMMENT '用户ID',
  `code` varchar(20) DEFAULT '' COMMENT '发送邮件内容',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否使用，默认0，0为未使用，1为使用',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件发送记录表';

-- ----------------------------
--  Table structure for `pcf_soft_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_soft_content`;
CREATE TABLE `pcf_soft_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `typeid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `content` longtext NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='下载表';

-- ----------------------------
--  Table structure for `pcf_upgrade`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_upgrade`;
CREATE TABLE `pcf_upgrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ver` varchar(20) DEFAULT NULL,
  `content` mediumtext,
  `add_time` bigint(20) DEFAULT '0',
  `files` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='升级记录表';

-- ----------------------------
--  Table structure for `pcf_users`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_users`;
CREATE TABLE `pcf_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_id` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '默认会员等级为注册会员',
  `open_id` varchar(30) NOT NULL DEFAULT '' COMMENT '微信唯一标识openid',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `truename` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `is_mobile` tinyint(1) DEFAULT '0' COMMENT '绑定手机号，0为不绑定，1为绑定',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号码',
  `is_email` tinyint(1) DEFAULT '0' COMMENT '绑定邮箱，0为不绑定，1为绑定',
  `email` varchar(60) DEFAULT '' COMMENT 'email',
  `qq` varchar(13) NOT NULL DEFAULT '' COMMENT 'qq号码',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `litpic` varchar(255) DEFAULT '' COMMENT '头像',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `login_time` int(11) DEFAULT '0' COMMENT '登陆时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `register_place` tinyint(1) DEFAULT '2' COMMENT '注册位置。后台注册不受注册验证影响，1为后台注册，2为前台注册。默认为2。',
  `admin_id` int(10) DEFAULT '0' COMMENT '关联管理员ID',
  `remark` varchar(255) DEFAULT NULL,
  `is_realname` tinyint(1) DEFAULT '0',
  `price` int(20) DEFAULT '0',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
--  Table structure for `pcf_users_level`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_users_level`;
CREATE TABLE `pcf_users_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `free_day_send` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '每天免费发布',
  `free_all_send` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '共免费发布',
  `fee_day_top` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '每天免费置顶',
  `fee_all_top` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '共免费置顶',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态（1：启用，0：关闭）',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '注册默认级别',
  `sort_order` int(10) DEFAULT NULL,
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='会员级别表';

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `pcf_arcrank` VALUES ('1','0','开放浏览','0','1552376880','cn'), ('2','-1','待审核稿件','0','1552376880','cn');
INSERT INTO `pcf_channelfield` VALUES ('1','add_time','0','新增时间','datetime','int(11)','250','','0','0','1','0','1','1','1','100','1','1533091575','1533091575'), ('2','update_time','0','更新时间','datetime','int(11)','250','','0','0','1','0','1','1','1','100','1','1533091601','1533091601'), ('3','aid','0','文档ID','int','int(11)','250','','0','0','1','0','1','1','1','100','1','1533091624','1533091624'), ('4','typeid','0','当前栏目ID','int','int(11)','250','','0','0','1','0','1','1','1','100','1','1533091930','1533091930'), ('5','channel','0','模型ID','int','int(11)','250','','0','0','1','0','1','1','1','100','1','1533092214','1533092214'), ('6','is_b','0','是否加粗','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092246','1533092246'), ('7','title','0','文档标题','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092381','1533092381'), ('8','litpic','0','缩略图','img','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092398','1533092398'), ('9','is_head','0','是否头条','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092420','1533092420'), ('10','is_special','0','是否特荐','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092439','1533092439'), ('11','is_top','0','是否置顶','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092454','1533092454'), ('12','is_recom','0','是否推荐','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092468','1533092468'), ('13','is_jump','0','是否跳转','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092484','1533092484'), ('14','author','0','编辑者','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092498','1533092498'), ('15','click','0','浏览量','int','int(11)','250','','0','0','1','0','1','1','1','100','1','1533092512','1533092512'), ('16','arcrank','0','阅读权限','select','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092534','1533092534'), ('17','jumplinks','0','跳转链接','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092553','1533092553'), ('19','seo_title','0','SEO标题','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092713','1533092713'), ('20','seo_keywords','0','SEO关键词','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092725','1533092725'), ('21','seo_description','0','SEO描述','text','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533092739','1533092739'), ('22','status','0','状态','switch','tinyint(1)','250','','0','0','1','0','1','1','1','100','1','1533092753','1533092753'), ('23','sort_order','0','排序号','int','int(11)','250','','0','0','1','0','1','1','1','100','1','1533092766','1533092766'), ('27','content','6','内容详情','htmltext','longtext','0','','0','0','0','0','0','1','0','1','1','1533464715','1616029525'), ('29','content','1','内容详情','htmltext','longtext','0','','0','0','0','0','0','1','0','10','1','1533464713','1599103785'), ('30','update_time','-99','更新时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('31','add_time','-99','新增时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('32','status','-99','启用 (1=正常，0=屏蔽)','switch','tinyint(1)','1','1','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('33','is_part','-99','栏目属性：0=内容栏目，1=外部链接','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('34','is_hidden','-99','是否隐藏栏目：0=显示，1=隐藏','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('35','sort_order','-99','排序号','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('36','seo_description','-99','SEO描述','multitext','text','0','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('37','seo_keywords','-99','SEO关键字','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('38','seo_title','-99','SEO标题','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('39','tempview','-99','文档模板文件名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('40','templist','-99','列表模板文件名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('41','litpic','-99','栏目图片','img','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('42','typelink','-99','栏目链接','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('43','grade','-99','栏目等级','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('44','englist_name','-99','栏目英文名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('45','dirpath','-99','目录存放HTML路径','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('46','dirname','-99','目录英文名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('47','typename','-99','栏目名称','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('48','parent_id','-99','栏目上级ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('49','current_channel','-99','栏目当前模型ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('50','channeltype','-99','栏目顶级模型ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('51','id','-99','栏目ID','int','int(10)','10','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('176','update_time','1','更新时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('177','add_time','1','新增时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('180','admin_id','1','管理员ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('182','sort_order','1','排序号','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('183','status','1','状态(0=屏蔽，1=正常)','switch','tinyint(1)','1','1','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('184','tempview','1','文档模板文件名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('187','seo_description','1','SEO描述','multitext','text','0','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('188','seo_keywords','1','SEO关键词','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('189','seo_title','1','SEO标题','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('191','jumplinks','1','外链跳转','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('192','arcrank','1','阅读权限：0=开放浏览，-1=待审核稿件','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('193','click','1','浏览量','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('194','author','1','作者','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('195','is_litpic','1','图片（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('196','is_jump','1','跳转链接（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('197','is_recom','1','推荐（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('198','is_top','1','置顶（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('199','is_special','1','特荐（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('200','is_head','1','头条（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('201','litpic','1','缩略图','img','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('202','title','1','标题','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('203','is_b','1','加粗','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('204','channel','1','模型ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('205','typeid','1','当前栏目','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('206','aid','1','aid','int','int(10)','10','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('326','joinaid','1','关联文档id','int','int(10) unsigned','10','0','0','0','1','0','1','1','1','100','1','1568962806','1568962806'), ('426','admin_id','-99','管理员ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569036451','1569036451'), ('467','update_time','6','更新时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('468','add_time','6','新增时间','datetime','int(11)','11','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('471','admin_id','6','管理员ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('472','sort_order','6','排序号','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('473','status','6','状态(0=屏蔽，1=正常)','switch','tinyint(1)','1','1','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('474','tempview','6','文档模板文件名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('477','seo_description','6','SEO描述','multitext','text','0','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('478','seo_keywords','6','SEO关键词','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('479','seo_title','6','SEO标题','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('481','jumplinks','6','外链跳转','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('482','arcrank','6','阅读权限：0=开放浏览，-1=待审核稿件','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('483','click','6','浏览量','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('484','author','6','作者','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('485','is_litpic','6','图片（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('486','is_jump','6','跳转链接（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('487','is_recom','6','推荐（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('488','is_top','6','置顶（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('489','is_special','6','特荐（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('490','is_head','6','头条（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('491','litpic','6','缩略图','img','varchar(250)','250','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('492','title','6','标题','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('493','is_b','6','加粗','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('494','channel','6','模型ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('495','typeid','6','当前栏目','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('496','joinaid','6','关联文档id','int','int(10) unsigned','10','0','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('497','aid','6','aid','int','int(10)','10','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('521','admin_id','0','管理员ID','int','int(10)','10','0','0','0','1','0','1','1','1','100','1','1570500751','1570500751');
INSERT INTO `pcf_channelfield` VALUES ('522','tempview','0','文档模板文件名','text','varchar(200)','200','','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('523','is_litpic','0','图片（0=否，1=是）','switch','tinyint(1)','1','0','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('524','joinaid','0','关联文档id','int','int(10) unsigned','10','0','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('540','content','7','内容','htmltext','longtext','0','','0','0','0','0','0','1','0','12','1','1616146306','1617443321'), ('552','content','8','内容','htmltext','longtext','0','','0','0','0','0','0','1','0','20','1','1619334391','1619405378'), ('553','content','9','内容','htmltext','longtext','0','','0','0','0','0','0','1','0','100','1','1619334406','1619334406'), ('554','content','10','内容','htmltext','longtext','0','','0','0','0','0','0','1','0','100','1','1619334418','1619336595'), ('555','note','8','描述','multitext','text','0','','0','0','0','0','0','1','0','10','1','1619405333','1619405375');
INSERT INTO `pcf_channelfield_bind` VALUES ('21','0','27','1','1616029525','1616029525'), ('23','0','540','1','1616146306','1616146306'), ('25','0','541','1','1617023454','1617023454'), ('26','0','542','1','1617023512','1617023512'), ('27','0','543','1','1617023773','1617023773'), ('28','0','544','1','1617025286','1617025286'), ('29','0','545','1','1617025304','1617025304'), ('30','0','546','1','1617025318','1617025318'), ('31','0','547','1','1617025337','1617025337'), ('32','0','548','1','1617025360','1617025360'), ('36','0','552','1','1619334391','1619334391'), ('37','0','553','1','1619334406','1619334406'), ('39','0','554','1','1619336595','1619336595'), ('40','0','555','1','1619405333','1619405333');
INSERT INTO `pcf_channel_type` VALUES ('1','article','新闻模型','新闻','article','Article','1','1','1','20','layui-icon-list','1615789797','1615472402','cn'), ('6','single','单页模型','单页','single','Single','1','1','1','10','layui-icon-template-1','1615968413','1615789061','cn'), ('7','product','产品模型','产品','product','Product','1','1','1','30','layui-icon-cart','1619331601','1616811640','cn'), ('8','picture','图片模型','图片','picture','Picture','1','1','1','40','layui-icon-picture','1616820307','1619334073','cn'), ('9','soft','下载模型','下载','soft','Soft','1','1','1','50','layui-icon-download-circle','1616820286','1619334073','cn'), ('10','media','视频模型','视频','media','Media','1','1','1','60','layui-icon-video','1616141303','1619334072','cn');
INSERT INTO `pcf_config` VALUES ('1','is_mark','0','water','','1607739250','0','cn'), ('2','mark_txt','网站建设','water','','1619402609','0','cn'), ('3','mark_img','','water','','1586694527','0','cn'), ('4','mark_width','200','water','','1586694527','0','cn'), ('5','mark_height','200','water','','1578880113','0','cn'), ('6','mark_degree','34','water','','1615379325','0','cn'), ('7','mark_quality','34','water','','1582942229','0','cn'), ('8','mark_sel','9','water','','1607672161','0','cn'), ('11','file_size','100','basic','','1619331255','0','cn'), ('12','image_type','jpg|gif|png|bmp|jpeg|ico','basic','','1578963547','0','cn'), ('13','file_type','zip|gz|rar|iso|doc|xsl|ppt|wps','basic','','1586694527','0','cn'), ('14','media_type','swf|mpg|mp3|rm|rmvb|wmv|wma|wav|mid|mov','basic','','1615379215','0','cn'), ('15','web_keywords','','web','','1629790097','0','cn'), ('18','seo_viewtitle_format','3','seo','','1590808402','0','cn'), ('24','mark_type','text','water','','1615379318','0','cn'), ('25','mark_txt_size','30','water','','1586694527','0','cn'), ('26','mark_txt_color','#358c1f','water','','1615687823','0','cn'), ('28','web_name','','web','','1629790097','0','cn'), ('29','web_logo','','web','','1629790097','0','cn'), ('30','web_ico','/static/img/favicon.ico','web','','1619344221','0','cn'), ('31','web_basehost','','web','','1619335629','0','cn'), ('32','web_description','','web','','1629790097','0','cn'), ('33','web_copyright','','web','','1619331551','0','cn'), ('34','web_thirdcode_pc','','web','','1619331522','0','cn'), ('35','web_thirdcode_wap','','web','','1616565347','0','cn'), ('40','seo_pseudo','2','seo','','1619420037','0','cn'), ('41','list_symbol',' > ','basic','','1578641191','0','cn'), ('42','sitemap_auto','1','sitemap','','1616054831','0','cn'), ('47','sitemap_zzbaidutoken','','sitemap','','1596073835','0','cn'), ('55','web_title','','web','','1629790097','0','cn'), ('62','seo_inlet','1','seo','','1569719545','0','cn'), ('65','web_cmsurl','http://www.pcfcms.com','web','','1586000604','0','cn'), ('66','web_templets_dir','/template/default','web','','1588835254','0','cn'), ('67','web_templeturl','/template/default','web','','1588835254','0','cn'), ('68','web_templets_pc','/template/default/pc','web','','1588835254','0','cn'), ('69','web_templets_m','/template/default/mobile','web','','1588835254','0','cn'), ('70','web_pcfcms','http://www.pcfcms.com','web','','1608097533','0','cn'), ('76','seo_liststitle_format','2','seo','','1608000119','0','cn'), ('77','web_status','1','web','','1617249583','0','cn'), ('79','web_recordnum','','web','','1629790097','0','cn'), ('80','web_is_authortoken','-1','web','','1596176181','0','cn'), ('81','web_adminbasefile','/login.php','web','','1619337056','0','cn'), ('82','seo_rewrite_format','2','seo','','1619420256','0','cn'), ('89','system_sql_mode','NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION','system','','1570841249','0','cn'), ('174','web_is_https','0','web','','1578966774','0','cn'), ('190','system_auth_code','FBWQg3f7Psur9E6Usp8j','system','','1557462848','0','cn'), ('193','system_version','v3.0','system','','1604912373','0','cn'), ('207','system_correctarctypedirpath','1','system','','1562052310','0','cn'), ('209','thumb_open','0','thumb','','1615380301','0','cn'), ('210','thumb_mode','3','thumb','','1585555509','0','cn'), ('211','thumb_color','#FFFFFF','thumb','','1562313846','0','cn'), ('212','thumb_width','300','thumb','','1562313846','0','cn'), ('213','thumb_height','300','thumb','','1562313846','0','cn'), ('220','web_templets_mobile','/template/default/mobile','web','','1586694527','0','cn'), ('234','basic_indexname','网站首页','basic','','1578641388','0','cn'), ('235','max_filesize','100','basic','','1578641191','0','cn'), ('236','max_sizeunit','MB','basic','','1568967416','0','cn'), ('238','system_tpl_theme','default','system','','1615688435','0','cn'), ('241','web_mobile_domain_open','0','web','','1616562065','0','cn'), ('242','web_mobile_domain','m','web','','1588491084','0','cn'), ('245','web_adminlogo','/admin/assets/images/logo.png','web','','1582614294','0','cn'), ('246','old_web_adminlogo','/admin/assets/images/logo.png','web','','1582614323','0','cn'), ('252','system_channeltype_unit','1','system','','1572340836','0','cn'), ('262','is_thumb_mark','0','thumb','','1615380301','0','cn'), ('266','sitemap_archives_num','100','sitemap','','1593671025','0','cn'), ('278','smtp_syn_weapp','0','smtp','','1614396658','0','cn'), ('279','smtp_server','smtp.qq.com','smtp','','1586694527','0','cn'), ('280','smtp_port','465','smtp','','1590457867','0','cn'), ('281','smtp_user','123456789@qq.com','smtp','','1619337228','0','cn'), ('282','smtp_pwd','123456789','smtp','','1619337228','0','cn'), ('283','smtp_test','','smtp','','1593327886','0','cn'), ('294','sms_syn_weapp','0','sms','','1619336963','0','cn'), ('295','sms_appkey','123456789','sms','','1619336958','0','cn'), ('296','sms_secretkey','123456789','sms','','1619336958','0','cn'), ('297','sms_test','','sms','','1595988925','0','cn'), ('302','sms_content','','sms','','1593333167','0','cn'), ('315','sms_platform','2','sms','','1617345183','0','cn'), ('319','sitemap_xml','1','sitemap','','1593670987','0','cn'), ('321','oss_switch','0','oss','','1586694527','0','cn'), ('322','adminbasefile_old','login','web','','1619337064','0','cn'), ('323','adminbasefile','login','web','','1619337053','0','cn'), ('339','old_web_logo','/uploads/allimg/20210426/629dc593bd5ad4375e94fef3fadc8a05.jpg','web','','1619407148','0','cn'), ('340','users_reg_notallow','www,bbs,ftp,mail,user,users,admin,administrator,pcfcms','users_config','不允许注册的会员名','0','0','cn'), ('341','users_verification','0','users_config','验证方式','1619336922','0','cn'), ('342','users_open','0','users_config','开放会员','1619402626','0','cn'), ('343','users_open_reg','0','users_config','开放注册','1619402626','0','cn'), ('344','users_open_login','0','users_config','开放登陆','1619402626','0','cn'), ('345','userspass_verification','0','users_config','找回密码','1619336922','0','cn');
INSERT INTO `pcf_field_type` VALUES ('checkbox','多选项','1','1','1532485708','1532485708'), ('datetime','日期和时间','0','12','1532485708','1532485708'), ('decimal','金额类型','0','9','1532485708','1532485708'), ('float','小数类型','0','10','1532485708','1532485708'), ('htmltext','HTML文本','0','8','1532485708','1532485708'), ('img','单张图','0','6','1532485708','1532485708'), ('imgs','多张图','0','7','1532485708','1532485708'), ('int','整数类型','0','11','1532485708','1532485708'), ('multitext','多行文本','0','5','1532485708','1532485708'), ('radio','单选项','1','2','1532485708','1532485708'), ('select','下拉框','1','3','1532485708','1532485708'), ('switch','开关','0','13','1532485708','1532485708'), ('text','单行文本','0','4','1532485708','1532485708');
INSERT INTO `pcf_guestbook` VALUES ('10','在线留言','0','1','1598880028','1615968273','cn');
INSERT INTO `pcf_guestbook_attr` VALUES ('41','你的姓名','10','text','','100','1','1598880028','1615968273','cn'), ('42','手机号码','10','text','','100','1','1598880028','1615968273','cn'), ('45','项目类型','10','text','','100','1','1606447424','1615968273','cn'), ('46','城市/省份','10','text','','100','1','1607408007','1615968273','cn');
INSERT INTO `pcf_menu` VALUES ('1','0','系统','layui-icon-set','','index/main','后台首页','1','1','1586691761','1615968437','1','1'), ('2','0','内容','layui-icon-read','','content/index','快速编辑','1','2','1586691778','1615985154','1','1'), ('3','0','用户','layui-icon-user','','admin/index','管理员','1','4','1586691792','1615986862','1','1'), ('4','1','系统配置','layui-icon-set-fill','','','','2','3','1586695887','1614003683','1','1'), ('5','13','网站设置','','web/index','','','3','1','1586696470','1615359514','1','1'), ('6','5','查看','','','','list','4','10','1586698676','1615363899','1','1'), ('7','5','修改','','','','modify','4','10','1586698981','1615359531','1','1'), ('8','5','删除','','','','delete','4','10','1586699004','1615359633','1','1'), ('10','4','数据备份','','tools/index','','','3','3','1586700790','1615040310','1','1'), ('12','4','系统升级','','upgrade/index','','','3','5','1586700856','1615040322','1','1'), ('13','1','全局管理','layui-icon-auz','','','','2','2','1586700909','1615359422','1','1'), ('14','4','节点管理','','sysmenu/index','','','3','1','1586700974','1615040304','1','1'), ('15','205','管理员','','admin/index','','','3','1','1586701010','1615040352','1','1'), ('16','205','角色管理','','authrole/index','','','3','2','1586701032','1615040359','1','1'), ('19','3','会员','layui-icon-group','','','','2','2','1586701436','1614092045','1','1'), ('21','13','导航管理','','nav/index','','','3','2','1586702099','1615040273','1','1'), ('22','13','模型管理','','type/index','','','3','4','1586702124','1615040286','1','1'), ('24','13','广告管理','','adposition/index','','','3','3','1586702242','1615040277','1','1'), ('25','13','友情链接','','links/index','','','3','3','1586702256','1615040281','1','1'), ('26','206','表单功能','','guestbook/index','','','2','3','1586702273','1615040338','1','1'), ('28','13','URL配置','','adminseo/index','','','3','5','1586702305','1615040290','1','1'), ('29','13','网站地图','','sitemap/index','','','3','6','1586702321','1615040293','1','1'), ('30','13','文件管理','','res/index','','','3','6','1586702462','1615040296','1','1'), ('31','19','会员列表','','users/index','','','3','1','1586702532','1615040367','1','1'), ('32','19','会员等级','','userslevel/index','','','3','2','1586703709','1615040373','1','1'), ('38','10','查看','','','','list','4','10','1586745064','0','1','1'), ('39','10','备份','','','','export','4','10','1586745081','1588395699','1','1'), ('40','10','删除','','','','delete','4','10','1586745090','0','1','1'), ('44','12','查看','','','','list','4','10','1586745201','1586847976','1','1'), ('45','12','更新','','','','update','4','10','1586745216','1615358943','1','1'), ('46','30','查看','','','','list','4','10','1586745318','1596789269','1','1'), ('48','30','删除','','','','delete','4','10','1586745342','1596789270','1','1'), ('50','14','查看','','','','list','4','10','1586745391','0','1','1'), ('51','14','修改','','','','modify','4','10','1586745402','0','1','1'), ('52','14','删除','','','','delete','4','10','1586745417','0','1','1'), ('53','14','新增','','','','add','4','10','1586745433','0','1','1'), ('54','15','查看','','','','list','4','10','1586745452','0','1','1'), ('55','15','修改','','','','modify','4','10','1586745463','0','1','1'), ('56','15','删除','','','','delete','4','10','1586745475','0','1','1'), ('57','15','新增','','','','add','4','10','1586745489','0','1','1'), ('58','16','查看','','','','list','4','10','1586745508','1586747072','1','1'), ('59','16','修改','','','','modify','4','10','1586745520','1586747095','1','1'), ('60','16','删除','','','','delete','4','10','1586745533','1586747108','1','1'), ('61','16','新增','','','','add','4','10','1586745546','1586747121','1','1'), ('71','21','查看','','','','list','4','10','1586746060','1596784728','1','1'), ('72','21','修改','','','','modify','4','10','1586746072','1615358953','1','1'), ('73','21','删除','','','','delete','4','10','1586746085','1596784729','1','1'), ('74','21','新增','','','','add','4','10','1586746098','1596784729','1','1'), ('75','22','查看','','','','list','4','10','1586746116','0','1','1'), ('76','22','修改','','','','modify','4','10','1586746127','0','1','1'), ('77','22','删除','','','','delete','4','10','1586746140','0','1','1'), ('78','22','新增','','','','add','4','10','1586746160','1598951320','1','1'), ('81','24','查看','','','','list','4','10','1586746254','1586832574','1','1'), ('82','24','修改','','','','modify','4','10','1586746264','1586832607','1','1'), ('83','24','删除','','','','delete','4','10','1586746274','1586832625','1','1'), ('84','24','新增','','','','add','4','10','1586746292','1586832641','1','1'), ('85','25','查看','','','','list','4','10','1586746303','0','1','1'), ('86','25','修改','','','','modify','4','10','1586746313','0','1','1'), ('87','25','删除','','','','delete','4','10','1586746327','0','1','1'), ('88','25','新增','','','','add','4','10','1586746343','0','1','1'), ('89','26','查看','','','','list','4','10','1586746360','1586786337','1','1'), ('90','26','修改','','','','modify','4','10','1586746371','1586786337','1','1'), ('91','26','删除','','','','delete','4','10','1586746381','1586786338','1','1'), ('92','26','新增','','','','add','4','10','1586746398','1586786338','1','1'), ('97','28','查看','','','','list','4','10','1586746500','1598450096','1','1'), ('98','28','修改','','','','modify','4','10','1586746516','1598450097','1','1'), ('99','29','查看','','','','list','4','10','1586746607','1596079856','1','1'), ('100','29','修改','','','','modify','4','10','1586746622','1596079857','1','1'), ('101','31','查看','','','','list','4','10','1586746639','0','1','1'), ('102','31','修改','','','','modify','4','10','1586746648','0','1','1'), ('103','31','删除','','','','delete','4','10','1586746657','0','1','1'), ('104','31','新增','','','','add','4','10','1586746667','0','1','1'), ('105','32','查看','','','','list','4','10','1586746681','0','1','1'), ('106','32','修改','','','','modify','4','10','1586746691','0','1','1'), ('107','32','删除','','','','delete','4','10','1586746703','0','1','1'), ('108','32','新增','','','','add','4','10','1586746714','0','1','1'), ('118','1','后台首页','layui-icon-home','index/main','','','2','1','1586754893','1615359331','1','1'), ('145','5','新增','','','','add','4','10','1586837276','1615359337','1','1'), ('146','14','状态','','','','status','4','10','1587814987','0','1','1'), ('147','15','状态','','','','status','4','10','1587894599','0','1','1'), ('148','15','修改密码','','','','userinfo','4','10','1587954221','1614330359','1','1'), ('150','10','优化','','','','optimze','4','10','1588395730','0','1','1'), ('151','10','修复','','','','repair','4','10','1588395759','0','1','1'), ('152','10','恢复','','','','import','4','10','1588395843','0','1','1'), ('153','10','下载','','','','down','4','10','1588395864','0','1','1'), ('155','26','状态','','','','status','4','10','1588420041','0','1','1'), ('156','25','状态','','','','status','4','10','1588423866','0','1','1'), ('157','22','状态','','','','status','4','10','1588667166','0','1','1'), ('177','31','状态','','','','status','4','10','1591777892','0','1','1'), ('178','32','状态','','','','status','4','10','1591777909','0','1','1'), ('204','4','语言管理','','','','','3','10','1614003980','1614309474','0','1'), ('205','3','管理员','layui-icon-username','','','','2','1','1614004098','1614092039','1','1'), ('206','0','数据','layui-icon-template','','guestbook/index','表单功能','1','3','1614004222','1615359467','1','1'), ('208','206','操作日志','','adminlog/index','','','2','10','1614004334','1615040345','1','1'), ('209','13','地区管理','','','','','3','10','1614004734','1614309470','0','1'), ('210','208','删除','','','','delete','4','10','1614243999','1614244013','1','1'), ('211','2','查看','','','','list','4','10','1614309224','1614319721','1','1'), ('212','2','修改','','','','modify','4','10','1614309250','1614319736','1','1'), ('213','2','删除','','','','delete','4','10','1614309268','1614319744','1','1'), ('214','2','新增','','','','add','4','10','1614309289','1614319750','1','1');
INSERT INTO `pcf_menu` VALUES ('215','2','状态','','','','status','4','10','1614309311','1614319757','1','1'), ('216','21','状态','','','','status','4','10','1614329454','0','1','1'), ('217','24','状态','','','','status','4','10','1614329573','0','1','1'), ('218','208','查看','','','','list','4','9','1615375169','1615375187','1','1');
INSERT INTO `pcf_nav_position` VALUES ('1','PC端主导航','100','0','0','cn'), ('4','PC端底部导航','100','0','0','cn'), ('5','WAP端主导航','100','0','0','cn'), ('6','WAP端底部导航','100','0','0','cn');
INSERT INTO `pcf_quickentry` VALUES ('10','网站设置','网站设置','web','index','','0','1','1','1','1569232484','1617695499','cn'), ('13','数据备份','数据备份','tools','index','','0','1','1','7','1569232484','1617695499','cn'), ('14','URL配置','URL配置','adminseo','index','','0','1','1','6','1569232484','1617695499','cn'), ('15','模板管理','模板管理','filemanager','index','','0','1','1','9','1569232484','1617695499','cn'), ('17','模型管理','模型管理','type','index','','0','1','1','5','1569232484','1617695499','cn'), ('18','广告管理','广告管理','adposition','index','','0','1','1','3','1569232484','1617695499','cn'), ('19','友情链接','友情链接','links','index','','0','1','1','4','1569232484','1617695499','cn'), ('21','管理员','管理员','admin','index','','0','1','1','8','1569232484','1617695499','cn'), ('30','网站导航','网站导航','nav','index','','0','1','1','2','0','1617695499','cn'), ('31','节点管理','节点管理','sysmenu','index','','0','1','1','10','0','1617695499','cn');
INSERT INTO `pcf_users_level` VALUES ('1','一级会员','0','0','0','0','1','1619399653','0','0',NULL,'cn'), ('2','二级会员','0','0','0','0','1','1619399662','0','0',NULL,'cn'), ('3','三级会员','0','0','0','0','1','1619399672','0','0',NULL,'cn');
