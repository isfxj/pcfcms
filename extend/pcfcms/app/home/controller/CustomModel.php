<?php
/***********************************************************
 * 自定义
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;
use app\home\model\CustomModel as NewCustomModel;
use app\home\controller\Lists as Listscontroller;
use app\home\controller\View as Viewcontroller;
class CustomModel extends Base
{
    public $nid = 'custommodel';
    public $channelid = '';
    public $diymodel = '';
    public $Listscontroller = ''; 
    public $Viewcontroller = '';    
    public function _initialize() {
        parent::_initialize();
        $channeltype_list = config('pcfcms.channeltype_list');
        $this->diymodel = new NewCustomModel();
        $this->Listscontroller = new Listscontroller($this->app);
        $this->Viewcontroller = new Viewcontroller($this->app);
        if(!empty($channeltype_list)){
            $this->channelid = $channeltype_list[$this->nid];
        }else{
            $this->channelid= '';
        }
    }

    public function lists($tid)
    {
        $tid_tmp = $tid;
        $seo_pseudo = sysConfig('seo.seo_pseudo');
    	if (empty($tid)) {
            $map = [
                'channeltype'   => $this->channelid,
                'parent_id' => 0,
                'is_hidden' => 0,
                'status'    => 1,
            ];
    	} else {
            if (2 == $seo_pseudo) {
                $map = ['dirname'=> $tid];
            } else {
                if (!is_numeric($tid) || strval(intval($tid)) !== strval($tid)) {
                    $this->error('页面不存在');
                }
                $map = ['id'=> $tid];
            }
        }
        $row = Db::name('arctype')->field('id,dirname')->where($map)->order('sort_order asc')->limit(1)->find();
        $tid = !empty($row['id']) ? intval($row['id']) : 0;
        $dirname = !empty($row['dirname']) ? $row['dirname'] : '';
        if (2 == $seo_pseudo) {
            $tid = $dirname;
        } else {
            $tid = $tid_tmp;
        }
        return $this->Listscontroller->index($tid);
    }

    public function view($aid)
    {
        $result = $this->diymodel->getInfo($aid);
        if (empty($result)) {
            $this->error('页面不存在');
        } elseif ($result['arcrank'] == -1) {
            $this->error('待审核稿件，你没有权限阅读！');
        }
        // 外部链接跳转
        if ($result['is_jump'] == 1) {
            header('Location: '.$result['jumplinks']);
        }
        return $this->Viewcontroller->index($aid);
    }

}