<?php
/***********************************************************
 * 广告列表
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Ad extends Common
{
    //列表
    public function tableData($post){
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('ad')
              ->field($tableWhere['field'])
              ->where($tableWhere['where'])
              ->order($tableWhere['order'])
              ->paginate($limit)->toArray();
        $pcfdata = $list['data'];
        foreach ($pcfdata as $key => $value) {
            $pcfdata[$key]['add_time'] = pcftime($value['add_time']);
            $pcfdata[$key]['litpic'] = get_default_pic($value['litpic']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $pcfdata];
        return $result;
    }

    protected function pcftableWhere($post){
        $where = [];
        $where[] = ['pid','=',$post['id']];
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "sort_order asc";
        return $result;
    }

    //添加|编辑
    public function toAdd($data){
        $domain = Request::baseFile().'/adposition/ad_index/id/'.$data['pid'];
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])){
            $where = [];
            $where[] = ['title','=',$data['title']];
            $where[] = ['id', '<>', $data['id']];
            /*$count = Db::name('ad')->where($where)->count();
            if($count > 0){
                $result = ['status' => false, 'msg' => '该广告名称已存在，请检查'];
                return $result;  
            }*/
            if((strtotime($data['end_time']) - strtotime($data['start_time'])) < 0){
                $result = ['status' => false, 'msg' => '广告结束时间必须要大于开始时间'];
                return $result;
            }
            $add_data['id'] = $data['id'];
            $add_data['title'] = $data['title'];
            $add_data['litpic'] = $data['litpic'];
            $add_data['links'] = $data['links'];
            $add_data['link_man'] = $data['link_man'];
            $add_data['link_email'] = $data['link_email'];
            $add_data['link_phone'] = $data['link_phone'];
            $add_data['start_time'] = strtotime($data['start_time']);
            $add_data['end_time'] = strtotime($data['end_time']);
            $add_data['intro'] = $data['intro'];
            $add_data['sort_order'] = $data['sort_order'];
            $add_data['bgcolor'] = $data['bgcolor'];
            $add_data['target'] = $data['target'];
            $add_data['update_time'] = getTime();
            if (Db::name('ad')->save($add_data)) {
                $result = ['status' => true, 'msg' => '修改成功','url' => $domain];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '修改失败'];
                return $result;
            }
        }else {
            //判断名是否重复
            /*$info = Db::name('ad')->where('title', $data['title'])->find();
            if(isset($info) && !empty($info)){
                if ($data['title'] == $info['title']){
                    $result = ['status' => false, 'msg' => '标题已存在'];
                    return $result;
                }
            }*/
            if((strtotime($data['end_time']) - strtotime($data['start_time'])) < 0){
                $result = ['status' => false, 'msg' => '广告结束时间必须要大于开始时间'];
                return $result;
            }
            $add_data['pid'] = $data['pid'];
            $add_data['title'] = $data['title'];
            $add_data['litpic'] = $data['litpic'];
            $add_data['links'] = $data['links'];
            $add_data['link_man'] = $data['link_man'];
            $add_data['link_email'] = $data['link_email'];
            $add_data['link_phone'] = $data['link_phone'];
            $add_data['start_time'] = strtotime($data['start_time']) ;
            $add_data['end_time'] = strtotime($data['end_time']);
            $add_data['intro'] = $data['intro'];
            $add_data['sort_order'] = $data['sort_order'];
            $add_data['bgcolor'] = $data['bgcolor'];
            $add_data['target'] = $data['target'];
            $add_data['admin_id'] = session::get('admin_id');
            $add_data['add_time'] = getTime();
            if (Db::name('ad')->save($add_data)) {
                $result = ['status' => true, 'msg' => '添加成功','url' => $domain];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '添加失败'];
                return $result;
            }
        }
    }

}
