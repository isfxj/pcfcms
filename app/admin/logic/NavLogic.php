<?php
/***********************************************************
 * 导航栏目-业务逻辑
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\logic;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
class NavLogic
{
    //检测是否有子菜单
    public function hasChildren($id){
        if (is_array($id)) {
            $ids = array_unique($id);
            $row = Db::name('nav_list')
                    ->field('parent_id, count(nav_) AS total')
                    ->where('parent_id','IN',$ids)
                    ->group('parent_id')
                    ->column('parent_id');
            return $row;
        } else {
            $count = Db::name('nav_list')->where('parent_id', $id)->count('nav_id');
            return ($count > 0 ? 1 : 0);
        }
    }

    // 全部栏目
    public function GetAllArctype($type_id = 0){
        $where = [];
        //获取全部模型栏目
        $where1 = [];
        $where1[] = ['status','=', 1]; 
        $ChannelTypeData = Db::name('channel_type')->field('*')->where($where1)->select()->toArray();
        $ChannelTypeData = convert_arr_key($ChannelTypeData,"id");
        //所有栏目分类
        $arctype_max_level = intval(config('pcfcms.arctype_max_level'));//栏目最多级别
        $where[] = ['c.status','<>', 0];
        $where[] = ['c.is_hidden','<>', 1];
        $fields = "c.id, c.parent_id, c.current_channel, c.typename,c.dirname, c.grade, count(s.id) as has_children, '' as children";
        $res = Db::name('arctype')
                ->field($fields)
                ->alias('c')
                ->join('arctype s','s.parent_id = c.id','LEFT')
                ->where($where)
                ->group('c.id')
                ->order('c.parent_id asc, c.sort_order asc, c.id')
                ->select()->toArray();
        if (empty($res)) {return false;}
        //所有栏目列表进行层次归类
        $arr = group_same_key($res, 'parent_id');
        for ($i=0; $i < $arctype_max_level; $i++) {
            foreach ($arr as $key => $val) {
                foreach ($arr[$key] as $key2 => $val2) {
                    if (!isset($arr[$val2['id']])) {
                        $arr[$key][$key2]['has_children'] = 0;
                        continue;
                    }
                    $val2['children'] = $arr[$val2['id']];
                    $arr[$key][$key2] = $val2;
                }
            }
        }
        static $seo_pseudo = null;
        if (null === $seo_pseudo) {
            $seoConfig = sysConfig('seo');
            $seo_pseudo = !empty($seoConfig['seo_pseudo']) ? $seoConfig['seo_pseudo'] : config('pcfcms.admin_config.seo_pseudo');
        }
        static $domain = null;
        null === $domain && $domain = request::domain();
        //组装成层级下拉列表框
        $select_html = '<select name="type_id" id="type_id" lay-filter="type_id">';
        $select_html .= '<option id="arctype_default" value="0">请选择栏目</option>';
        foreach ($arr[0] AS $key => $val){
            if(2 == $seo_pseudo) {
                // 伪静态
                if($val['current_channel'] == 6){
                     $typeurl = '/'.$val['dirname'].'.html';
                }else{
                    if(1 == sysConfig('seo.seo_rewrite_format')){
                        $typeurl = '/'.$val['dirname'].'.html';
                    }elseif(2 == sysConfig('seo.seo_rewrite_format')){
                        $typeurl = '/'.$ChannelTypeData[$val['current_channel']]['nid'].'/'.$val['dirname'].'.html';
                    }
                }
            }else{
                $typeurl = $domain."/lists/index.html?tid={$val['id']}";//动态
            }
            $select_html .= '<option value="'.$val['id'].'" data-typeurl="'.$typeurl.'" data-typename="'.$val['typename'].'"';
            $select_html .= ($type_id == $val['id']) ? ' selected="ture"' : '';
            $select_html .= '>';
            if ($val['grade'] > 0){
                $select_html .= str_repeat('&nbsp;', $val['grade'] * 4);
            }
            $select_html .= htmlspecialchars(addslashes($val['typename'])) . '</option>';
            if (empty($val['children'])) {continue;}
            foreach ($arr[0][$key]['children'] as $key2 => $val2) {
                if(2 == $seo_pseudo) {
                    // 伪静态
                    if($val2['current_channel'] == 6){
                         $typeurl1 = '/'.$val2['dirname'].'.html';
                    }else{
                        if(1 == sysConfig('seo.seo_rewrite_format')){
                            $typeurl1 = '/'.$val2['dirname'].'.html';
                        }elseif(2 == sysConfig('seo.seo_rewrite_format')){
                            $typeurl1 = '/'.$ChannelTypeData[$val2['current_channel']]['nid'].'/'.$val2['dirname'].'.html';
                        }
                    }
                }else{
                    $typeurl1 = $domain."/lists/index.html?tid={$val2['id']}";//动态
                }
                $select_html .= '<option value="'.$val2['id'].'" data-typeurl="'.$typeurl1.'" data-typename="'.$val2['typename'].'"';
                $select_html .= ($type_id == $val2['id']) ? ' selected="ture"' : '';
                if (!empty($allow_release_channel) && !in_array($val2['current_channel'], $allow_release_channel)) {
                    $select_html .= ' disabled="true" style="background-color:#f5f5f5;"';
                }
                $select_html .= '>';
                if ($val2['grade'] > 0){
                    $select_html .= str_repeat('&nbsp;', $val2['grade'] * 4);
                }
                $select_html .= htmlspecialchars(addslashes($val2['typename'])) . '</option>';
                if (empty($val2['children'])) {
                    continue;
                }
                foreach ($arr[0][$key]['children'][$key2]['children'] as $key3 => $val3) {
                    if(2 == $seo_pseudo) {
                        // 伪静态
                        if($val3['current_channel'] == 6){
                             //处理单页
                             $typeurl2 = '/'.$val3['dirname'].'.html';
                        }else{
                            if(1 == sysConfig('seo.seo_rewrite_format')){
                                $typeurl2 = '/'.$val3['dirname'].'.html';
                            }elseif(2 == sysConfig('seo.seo_rewrite_format')){
                                $typeurl2 = '/'.$ChannelTypeData[$val3['current_channel']]['nid'].'/'.$val3['dirname'].'.html';
                            }
                        }
                    }else{
                        $typeurl2 = $domain."/lists/index.html?tid={$val2['id']}";//动态
                    }
                    $select_html .= '<option value="'.$val3['id'].'" data-typeurl="'.$typeurl2.'" data-typename="'.$val2['typename'].'"';
                    $select_html .= ($type_id == $val3['id']) ? ' selected="ture"' : '';
                    $select_html .= '>';
                    if ($val3['grade'] > 0){
                        $select_html .= str_repeat('&nbsp;', $val3['grade'] * 4);
                    }
                    $select_html .= htmlspecialchars(addslashes($val3['typename'])) . '</option>';
                }
            }
        }
        $select_html .= '</select>';
        return $select_html;
    }

    // 功能列表
    public function ForegroundFunction(){
        return [
            [
                'title' => '首页',
                'code'   => 'home_Index_index',
                'url'   => '/',
            ]
        ];
    }

    // 获取指定父级的子菜单
    public function getNavigList($position_id, $parent_id = 0){
        $row = Db::name('nav_list')
                ->where(['position_id'=> $position_id,'parent_id'=> $parent_id])
                ->select()->toArray();
        return $row;
    }

    /**
     * 获得指定菜单下的子菜单的数组
     * @param   int     $id       菜单的ID
     * @param   int     $selected 当前选中菜单的ID
     * @param   boolean $re_type  返回的类型: 值为真时返回下拉列表,否则返回数组
     * @param   int     $level    限定返回的级数。为0时返回所有级数
     * @param   array   $map      查询条件
     */
    public function nav_list($id = 0, $selected = 0, $re_type = true, $level = 0, $map = [], $is_cache = true){
        static $res = NULL;
        if ($res === NULL){
            $where = [];
            if (!empty($map)) {
                $where = array_merge($where, $map);
            }
            foreach ($where as $key => $val) {       
                $key_tmp = 'c.'.$val[0];
                $where[$key_tmp] = $val[2];
                unset($where[$key]);
            } 
            $fields = "c.*, count(s.nav_id) as has_children, '' as children";
            $res = DB::name('nav_list')
                 ->field($fields)
                 ->alias('c')
                 ->join('nav_list s','s.parent_id = c.nav_id','LEFT')
                 ->where($where)
                 ->order('c.parent_id asc, c.sort_order asc, c.nav_id')
                 ->group('c.nav_id')
                 ->select()->toArray();
            $res = convert_arr_key($res,'nav_id');
        }
        if (empty($res) == true){
            $res = NULL;
            return $re_type ? '' : [];
        }
        $options = $this->navig_options($id, $res); // 获得指定菜单下的子菜单的数组
        if ($level > 0)
        {
            if ($id == 0){
                $end_level = $level;
            }else{
                $first_item = reset($options); // 获取第一个元素
                $end_level  = $first_item['level'] + $level;
            }
            foreach ($options AS $key => $val)
            {
                if ($val['level'] >= $end_level)
                {
                    unset($options[$key]);
                }
            }
        }
        $pre_key = 0;
        foreach ($options AS $key => $value)
        {
            $options[$key]['has_children'] = 0;
            if ($pre_key > 0)
            {
                if ($options[$pre_key]['nav_id'] == $options[$key]['parent_id'])
                {
                    $options[$pre_key]['has_children'] = 1;
                }
            }
            $pre_key = $key;
        }
        if ($re_type == true)
        {
            $select = '';
            foreach ($options AS $var)
            {
                $select .= '<option value="' . $var['nav_id'] . '" ';
                $select .= ($selected == $var['nav_id']) ? "selected='true'" : '';
                $select .= '>';
                if ($var['level'] > 0)
                {
                    $select .= str_repeat('&nbsp;', $var['level'] * 4);
                }
                $select .= htmlspecialchars(addslashes($var['nav_name'])) . '</option>';
            }
            $res = NULL;
            return $select;
        }else{
            $res = NULL;
            return $options;
        }
    }

    /**
     * 过滤和排序所有文章菜单，返回一个带有缩进级别的数组
     * @param   int     $id     上级菜单ID
     * @param   array   $arr    含有所有菜单的数组
     * @param   int     $level  级别
     */
    public function navig_options($spec_id, $arr){
        static $cat_options = array();
        if (isset($cat_options[$spec_id])){
            $cat_options = array();
            return $cat_options[$spec_id];
        }
        if (!isset($cat_options[0])){
            $level = $last_id = 0;
            $options = $id_array = $level_array = array();
            while (!empty($arr))
            {
                foreach ($arr AS $key => $value)
                {
                    $id = $value['nav_id'];
                    if ($level == 0 && $last_id == 0)
                    {
                        if ($value['parent_id'] > 0)
                        {
                            break;
                        }
                        $options[$id]          = $value;
                        $options[$id]['level'] = $level;
                        $options[$id]['nav_id']    = $id;
                        $options[$id]['nav_name']  = $value['nav_name'];
                        unset($arr[$key]);
                        if ($value['has_children'] == 0)
                        {
                            continue;
                        }
                        $last_id  = $id;
                        $id_array = array($id);
                        $level_array[$last_id] = ++$level;
                        continue;
                    }
                    if ($value['parent_id'] == $last_id)
                    {
                        $options[$id]          = $value;
                        $options[$id]['level'] = $level;
                        $options[$id]['nav_name']    = $id;
                        $options[$id]['nav_name']  = $value['nav_name'];
                        unset($arr[$key]);
                        if ($value['has_children'] > 0)
                        {
                            if (end($id_array) != $last_id)
                            {
                                $id_array[] = $last_id;
                            }
                            $last_id    = $id;
                            $id_array[] = $id;
                            $level_array[$last_id] = ++$level;
                        }
                    }
                    elseif ($value['parent_id'] > $last_id)
                    {
                        break;
                    }
                }
                $count = count($id_array);
                if ($count > 1){
                    $last_id = array_pop($id_array);
                }elseif ($count == 1){
                    if ($last_id != end($id_array)){
                        $last_id = end($id_array);
                    }else{
                        $level = 0;
                        $last_id = 0;
                        $id_array = array();
                        continue;
                    }
                }
                if ($last_id && isset($level_array[$last_id])){
                    $level = $level_array[$last_id];
                }else{
                    $level = 0;
                    break;
                }
            }
            $cat_options[0] = $options;
        }else{
            $options = $cat_options[0];
        }
        if (!$spec_id){
            $cat_options = array();
            return $options;
        }else{
            if (empty($options[$spec_id])){
                $cat_options = array();
                return array();
            }
            $spec_id_level = $options[$spec_id]['level'];
            foreach ($options AS $key => $value)
            {
                if ($key != $spec_id){
                    unset($options[$key]);
                }else{
                    break;
                }
            }
            $spec_id_array = array();
            foreach ($options AS $key => $value)
            {
                if (($spec_id_level == $value['level'] && $value['nav_id'] != $spec_id) || ($spec_id_level > $value['level'])){
                    break;
                }else{
                    $spec_id_array[$key] = $value;
                }
            }
            $cat_options[$spec_id] = $spec_id_array;
            $cat_options = array();
            return $spec_id_array;
        }
    }

}