<?php
/***********************************************************
 * 后台路由规则
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
use think\facade\Route;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Request;
$seo = sysConfig('seo');
$seo_pseudo = $seo['seo_pseudo'];
$seo_rewrite_format = $seo['seo_rewrite_format'];
if ($seo_pseudo == 2){
     // 精简伪静态
    if ($seo_rewrite_format == 1) {
        // 列表页
        if(input('param.sitemap_xml') > 0){
            Route::get('<tid>$', 'Lists/index');
        }
        // 内容页
        Route::get('View/<dirname>/<aid>$', 'View/index');
        // 单页
        Route::get('single/<tid>$', 'Single/lists');
    }else{
        // 单页
        Route::get('<tid>$', 'Single/lists');
        // 模型
        $cacheKey = "admin_route_channeltype";
        $channeltype_row = Cache::get($cacheKey);
        if (empty($channeltype_row)) {
            $channeltype_row = Db::name('channel_type')->field('nid,ctl_name')->select()->toArray();
            Cache::tag('channeltype')->set($cacheKey, $channeltype_row, ADMIN_CACHE_TIME); 
        }
        foreach ($channeltype_row as $value) {
            if(input('param.sitemap_xml') > 0){
                // 自定义模型列表
                Route::get($value['nid'].'/<tid>$', $value['ctl_name'].'/lists');
            }
            // 自定义模型内容
            Route::get($value['nid'].'/<dirname>/<aid>$', $value['ctl_name'].'/view');
        }

        
    }
}