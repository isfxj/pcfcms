<?php
/***********************************************************
 * 友情链接
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use app\admin\model\Links as Linkmodel;
class Links extends Base
{
    public $Linkmodel;
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $this->Linkmodel = new Linkmodel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    //列表
    public function index(){ 
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->Linkmodel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    //添加
    public function add(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
           return $this->Linkmodel->toAdd(input('param.'));  
        }
        return $this->fetch();
    }
    
    //编辑
    public function edit(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            return $this->Linkmodel->toAdd(input('param.'));
        }
        $info = Db::name('links')->where('id',input('param.id/d'))->find();
        if(!$info)$info = "";
        $this->assign('info', $info);
        return $this->fetch();
    }

    // 删除
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('param.del_id/d');
            if (Db::name('links')->where("id",$id)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result;
            }
            return $result;
        }       
    }

    // 批量删除
    public function batch_del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id_arr = input('param.del_id/a');
            $id_arr = eyIntval($id_arr);
            if(is_array($id_arr) && !empty($id_arr)){
                foreach ($id_arr as $key => $val) {
                   Db::name('links')->where('id',$val)->delete();
                }
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '参数有误'];
                return $result;
            }
        }       
    }


}