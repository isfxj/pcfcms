<?php
/***********************************************************
 * 节点管理
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;
class Sysmenu extends Base
{
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index()
    {
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        return $this->fetch();
    }

    public function get_menu()
    {
        $list = Cache::get('getmenu');
        if(!$list){
            $list = Db::name('menu')->order('sort asc')->select()->toArray();
            foreach ($list as $key => $value) {
                if($value['type'] == 1 ||$value['type'] == 2 || $value['type'] == 3 || $value['type'] == 4){
                   $list[$key]['open'] = false;
                }else{
                   $list[$key]['open'] = true; 
                }
            }
            Cache::tag('getmenu_cache')->set('getmenu', $list, ADMIN_CACHE_TIME);
        }
        if($list){
            $result = ['code' => 0, 'msg' => 'ok','data' => $list];
            return json($result);            
        }else{
            $result = ['code' => 1, 'msg' => 'no','data' =>''];
            return json($result);       
        }
    }

    public function add()
    {
        if (Request::isPost()) { 
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            } 
            $post = input('param.');
            $add_data = array();
            if (!$post['name']){
                $result = ['status' => false, 'msg' => '请输入权限名称'];
                return json($result);
            }
            if (!$post['type']){
                $result = ['status' => false, 'msg' => '请选择权限类型'];
                return json($result);
            }
            $add_data['parent_id'] = isset($post['parentId']) && !empty($post['parentId']) ? $post['parentId']:'';//上级菜单id
            $add_data['name'] = $post['name'];//权限名称
            $add_data['icon'] = $post['icon'];//图标
            $add_data['url'] = $post['url'];//URL地址
            $add_data['type'] = $post['type'];//类型
            $add_data['auth'] = $post['auth'];//权限标识
            $add_data['sort'] = isset($post['sort']) && !empty($post['sort']) ? $post['sort'] : '10';//显示顺序 
            $add_data['param'] = $post['param'];//参数
            $add_data['is_show'] = $post['is_show1'];//状态
            $add_data['create_time'] = getTime(); //时间 
            if (Db::name('menu')->save($add_data)) {
                Cache::clear('getmenu');
                $result = ['status' => true, 'msg' => '添加成功'];
                return json($result);
            } else {
                $result = ['status' => false, 'msg' => '添加失败'];
                return json($result);
            }
        }
    }

    public function edit()
    {
        if (Request::isPost()) { 
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            } 
            $post = input('param.');
            $add_data = array();
            if (!$post['name']){
                $result = ['status' => false, 'msg' => '请输入权限名称'];
                return json($result);
            }
            if (!$post['type']){
                $result = ['status' => false, 'msg' => '请选择权限类型'];
                return json($result);
            }
            $add_data['id'] = $post['id'];
            $add_data['parent_id'] = isset($post['parentId']) && !empty($post['parentId']) ? $post['parentId']:'';//上级菜单id
            $add_data['name'] = $post['name'];//权限名称
            $add_data['icon'] = $post['icon'];//图标
            $add_data['url'] = $post['url'];//URL地址
            $add_data['type'] = $post['type'];//类型
            $add_data['auth'] = $post['auth'];//权限标识
            $add_data['sort'] = isset($post['sort']) && !empty($post['sort']) ? $post['sort']:'10';//显示顺序 
            $add_data['param'] = $post['param'];//参数
            if(!empty($post['is_show']) || $post['is_show'] != ''){
                $add_data['is_show'] = $post['is_show'];//状态
            }
            $add_data['update_time'] = getTime(); //时间 
            if (Db::name('menu')->save($add_data)) {
                Cache::clear('getmenu');
                $result = ['status' => true, 'msg' => '修改成功'];
                return json($result);
            } else {
                $result = ['status' => false, 'msg' => '修改失败'];
                return json($result);
            }
        }
    }

    public function del()
    {
        if (Request::isPost()) {
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            }
            $post = input('param.');
            $id = $post['id'];
            $grade = Db::name('menu')->where('id','=',$id)->value('type');
            if($grade == 1){
                $map1=[];$map2=[];
                $map1=['type','=',2];
                $map1=['parent_id','=',$id];
                $map2=['type','=',3];
                $map2=['parent_id','=',$id];
                $list = Db::name('menu')->field('id')->whereOr([$map1, $map2])->select()->toArray();
                if(Db::name('menu')->where('id|parent_id','=',$id)->delete()){
                    foreach ($list as $key => $value) {
                       Db::name('menu')->where('parent_id',$value['id'])->delete();
                    }
                    Cache::clear('getmenu');
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => '删除失败'];
                    return $result;    
                }
            }if($grade == 2){
                $map=['type','=',3];
                $map=['parent_id','=',$id];
                $list = Db::name('menu')->field('id')->where($map)->select()->toArray();
                if(Db::name('menu')->where('id|parent_id','=',$id)->delete()){
                    foreach ($list as $key => $value) {
                       Db::name('menu')->where('parent_id',$value['id'])->delete();
                    }
                    Cache::clear('getmenu');
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => '删除失败'];
                    return $result;    
                }
            }else{
                if (Db::name('menu')->where('id|parent_id','=',$id)->delete()) {
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result;
                } else {
                    $result = ['status' => false, 'msg' => '删除失败'];
                    return $result;
                }                
            }

        } 
    }

}