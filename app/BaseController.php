<?php
/***********************************************************
 * 基础控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app;
use think\facade\App;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\facade\View;
use think\facade\Validate;
use think\facade\Request;
use traits\Jump;

// 控制器基础类
abstract class BaseController
{
    use Jump;
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    //CMS版本号 
    public $version = null;

    //CMS后台入口文件
    public $adminurl = null;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->request = app('request');
        $this->_initialize();
    }

    //初始化
    protected function _initialize()
    {
        if(App('http')->getName() == "home"){
            $this->pc_to_wap($this->request); //手机端访问自动跳到手机独立域名           
        }
        //版本号
        null === $this->version && $this->version = getCmsVersion();
        $this->assign('version', $this->version); 
        //CMS后台入口路径
        $web_adminbasefile = sysConfig('web.web_adminbasefile');
        $this->assign('adminurl', $this->request->domain().$web_adminbasefile);
    }

    //手机端访问自动跳到手机独立域名
    private function pc_to_wap($request = null)
    {
        $web_mobile_domain_open = sysConfig('web.web_mobile_domain_open'); // 是否开启手机域名访问
        $mobileurl = '';
        $subDomain = Request::subDomain(); //获取当前访问域名头
        $web_mobile_domain = sysConfig('web.web_mobile_domain'); // 二级域名头
        if((!empty($web_mobile_domain_open) && $web_mobile_domain_open)){
            // 浏览器手机模式访问
            if ($this->request->isMobile() && ($subDomain != $web_mobile_domain)) {
                if (!empty($web_mobile_domain) && $web_mobile_domain && $web_mobile_domain !='www') { 
                    $mobileurl = $request->scheme().'://'.$web_mobile_domain.'.'.$request->rootDomain().$request->url();
                }
                if (!empty($mobileurl)) {
                    header('Location: '.$mobileurl);
                }
            }
        }
    }

    /**
     * 验证数据
     * @access protected
     * @param array $data 数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array $message 提示信息
     * @param bool $batch 是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                list($validate, $scene) = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }
        $v->message($message);
        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }
        return $v->failException(true)->check($data);
    }

    protected function assign(...$vars)
    {
        View::assign(...$vars);
    }

    protected function fetch(string $template = '')
    {
        return View::fetch($template);
    }
    
    protected function redirect(...$args){
        throw new HttpResponseException(redirect(...$args));
    }

}
