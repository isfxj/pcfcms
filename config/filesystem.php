<?php
//图片存储地址
return [
    // 默认磁盘
    'default' => 'local',
    //磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => WWW_ROOT.'public/uploads/', 
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'oss',
            // 磁盘路径
            'root' => WWW_ROOT.'public/uploads/storage',
            // 磁盘路径对应的外部URL路径
            'url'        => request()->domain().'/storage',
            // 可见性
            'visibility' => 'public',
        ]
    ],
];
